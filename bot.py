"""
    Arranca el bot de telegram que responde las preguntas
"""

# https://apps.timwhitlock.info/emoji/tables/unicode
import os
import time
from io import BytesIO
from urllib.parse import urljoin

import requests
import spacy
import telebot
from PIL import Image
from requests.exceptions import ReadTimeout
from spacy.language import Language
from spacy_cld import LanguageDetector
from telebot import types


@Language.factory('language_detector')
def language_detector(nlp, name):
    return LanguageDetector()


nlp = spacy.load('en_core_web_sm')
nlp.add_pipe('language_detector', last=True)


TELEGRAM_TOKEN = '2002292214:AAESidPF0_EdheJbUzhP5UFblfrX3L98hxw'

bot = telebot.TeleBot(TELEGRAM_TOKEN)


def search_faqs(message):
    text = message.text

    docx = nlp(text)
    language = ''
    if docx._.languages:
        language = docx._.languages[0].split('-')[0]

    if language not in ['es', 'ar', 'zh', 'fr', 'ru', 'en']:
        language = 'es'

    print(
        text,
        docx._.languages
    )

    url = 'http://localhost:8000/search/'
    data = {
        "data":[text, ],
        "parameters": {
            "business": "pucp",
            "category": language,
            "flow_id": "1",
            "session_id": str(message.from_user.id)
        }
    }

    response = requests.post(url, json=data)
    result = response.json()

    for match in result['data']['docs'][0]['matches']:
        question = match['tags'].get('question')
        answer = match['tags'].get('answer')

        bot.send_message(
            message.chat.id,
            question
        )
        bot.send_message(
            message.chat.id,
            answer
        )
        break

    defaults = {
        'es': 'Información basíca sobre la COVID-19',
        'en': 'Coronavirus disease (COVID-19)',
        'ar': 'مرض فيروس كورونا (كوفيد-19)',
        'zh': '2019冠状病毒病（COVID-19）基本信息',
        'fr': 'Основные сведения о COVID-19',
        'ru': "Maladie à coronavirus 2019 (COVID-19) : ce qu'il faut savoir",
    }

    feedback = {
        'es': '¿Información basíca sobre la COVID-19 fu útil?',
        'en': 'Coronavirus disease (COVID-19)?',
        'ar': 'مرض فيروس كورونا (كوفيد-19)?',
        'zh': '2019冠状病毒病（COVID-19）基本信息?',
        'fr': 'Основные сведения о COVID-19?',
        'ru': "Maladie à coronavirus 2019 (COVID-19) : ce qu'il faut savoir?",
    }
    if result['data']['docs'][0]['matches']:
        markup = types.ReplyKeyboardMarkup()
        itembtn1 = types.KeyboardButton('Si')
        itembtn2 = types.KeyboardButton('No')
        itembtn3 = types.KeyboardButton('No sé')
        markup.row(itembtn1, itembtn2, itembtn3)
        bot.send_message(
            message.chat.id,
            feedback[language],
            reply_markup=markup
        )
    else:
        bot.send_message(
            message.chat.id,
            defaults[language]
        )


def recibe(messages):
    for message in messages:
        search_faqs(message)


bot.set_update_listener(recibe)


if __name__ == "__main__":
    print('Start...')
    bot.polling(none_stop=True)
