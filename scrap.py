"""
    Descarga el dataset de preguntas y respuestas del la web de la
    Organización Mundial De La Salud
"""
import csv

import requests
from bs4 import BeautifulSoup


def get_soup(url):
    response = requests.get(url)
    return BeautifulSoup(response.text, 'lxml')


def get_faqs(url, languaje):
    new_faqs = []
    soup = get_soup(url)
    for tag in soup.find_all('div', itemtype='https://schema.org/Question'):
        print('================')
        question = tag.find('span', itemprop='name').text.strip()
        print(question)
        answer = tag.find('div', itemprop='text').text.strip()
        print(answer)
        text = '{}\n{}'.format(question, answer)
        new_faqs.append(
            [
                text,
                question,
                answer,
                'pucp',
                languaje,
            ]
        )
    return new_faqs


def main():
    faqs = [['text', 'question', 'answer', 'business', 'category'], ]
    languajes = ['es', 'ar', 'zh', 'fr', 'ru', 'en']

    for languaje in languajes:
        url = 'https://www.who.int/{}/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub/q-a-detail/coronavirus-disease-covid-19'.format(
            languaje
        )
        new_faqs = get_faqs(url, languaje)
        faqs = faqs + new_faqs

        url = 'https://www.who.int/{}/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub'.format(
            languaje
        )
        soup = get_soup(url)
        for link in soup.find_all('a', class_='sf-list-vertical__item'):
            print('> > ', url, ' < <')
            url = 'https://www.who.int/{}'.format(link['href'])
            new_faqs = get_faqs(url, languaje)
            faqs = faqs + new_faqs

    with open('dataset.csv', 'w') as dataset:
        # using csv.writer method from CSV package
        write = csv.writer(dataset)

        # write.writerow(fields)
        write.writerows(faqs)


if __name__ == '__main__':
    main()

